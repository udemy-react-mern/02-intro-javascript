// Manejo de objetos

const persona = {
  nombre: 'Pau',
  apellido: 'Guerrero',
  edad: 26,
  direccion: {
    ciudad: 'Mexico',
    zip: 12345,
    lat: 14.3232,
    lng: 34.9233321
  }
};

// Se copia la referencia del espacio de la variable const persona2 = persona;

const persona2 = { ...persona } ;

persona2.nombre = 'otro nombre';

console.log(persona,persona2);